//
//  UsersRow.swift
//  CloneTwitter
//
//  Created by Rizky Ian Indiarto on 9/10/22.
//

import SwiftUI

struct UsersRow: View {
    var body: some View {
        HStack(spacing: 12) {
            Circle().frame(width: 48, height: 48)
            VStack(alignment: .leading, spacing: 4) {
                Text("Joker").font(.subheadline).bold().foregroundColor(.black)
                Text("Heath Ledger").font(.subheadline).foregroundColor(Color.gray)
            }
            
            Spacer()
        }.padding(.horizontal)
            .padding(.vertical, 4)
    }
}

struct UsersRow_Previews: PreviewProvider {
    static var previews: some View {
        UsersRow()
    }
}
