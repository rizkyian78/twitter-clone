//
//  TweetRowView.swift
//  CloneTwitter
//
//  Created by Rizky Ian Indiarto on 9/8/22.
//

import SwiftUI

struct TweetRowView: View {
    var body: some View {
        VStack(alignment: .leading) {
            HStack(alignment: .top, spacing: 12) {
                Circle()
                    .frame(width: 56, height: 56).foregroundColor(Color(.systemBlue))
//                User info and tweet
                VStack(alignment: .leading, spacing: 4 ) {
                    
                    HStack {
                        
                        Text("Bruce Wayne").font(.subheadline).bold()
                        Text("@Batman").foregroundColor(.gray).font(.caption)
                        Text("2 weeks ago").foregroundColor(.gray).font(.caption)
                    }
                    Text("I Believe in Harley Death").font(.subheadline).multilineTextAlignment(.leading)
                }
            }
            
//                action buttons
            HStack {
                Button  {
                    //
                } label: {
                    Image(systemName: "bubble.left").font(.subheadline)
                }
                Spacer()
                
                    Button  {
                        //
                    } label: {
                        Image(systemName: "arrow.2.squarepath").font(.subheadline)
                    }
                Spacer()
                    Button  {
                        //
                    } label: {
                        Image(systemName: "heart").font(.subheadline)
                    }
                Spacer()
                    Button  {
                        //
                    } label: {
                        Image(systemName: "bookmark").font(.subheadline)
                    }
            }
            .padding().foregroundColor(.gray)
            Divider()
        }
    }
}

struct TweetRowView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            TweetRowView()
            TweetRowView().preferredColorScheme(.dark)
        }
    }
}
