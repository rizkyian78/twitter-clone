//
//  NotificationView.swift
//  CloneTwitter
//
//  Created by Rizky Ian Indiarto on 9/9/22.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
