//
//  ExploreView.swift
//  CloneTwitter
//
//  Created by Rizky Ian Indiarto on 9/9/22.
//

import SwiftUI

struct ExploreView: View {
    var body: some View {
        NavigationView {
            VStack {
                ScrollView {
                    LazyVStack {
                        ForEach(0...25, id: \.self) {
                            _ in
                            NavigationLink {
                                ProfileView()
                            } label: {
                                UsersRow()
                            }
                        }
                    }
                }
            }.navigationTitle("Explore").navigationBarTitleDisplayMode(.inline)
        }
    }
}

struct ExploreView_Previews: PreviewProvider {
    static var previews: some View {
        ExploreView()
    }
}
