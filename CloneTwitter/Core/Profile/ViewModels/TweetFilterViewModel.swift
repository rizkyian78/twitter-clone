//
//  TweetFilterViewModel.swift
//  CloneTwitter
//
//  Created by Rizky Ian Indiarto on 9/10/22.
//

import Foundation

enum TweetFilterViewModel: Int, CaseIterable {
    case tweets
    case replies
    case likes
    
    var tittle: String {
        switch self {
            case .tweets: return "Tweets"
            case .replies: return "Replies"
            case .likes: return "Likes"
        }
    }
}
