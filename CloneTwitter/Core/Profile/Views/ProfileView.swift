//
//  ProfileView.swift
//  CloneTwitter
//
//  Created by Rizky Ian Indiarto on 9/9/22.
//

import SwiftUI

struct ProfileView: View {
    @State private var selectionFilter: TweetFilterViewModel = .tweets
    @Namespace var animation
    @Environment(\.presentationMode) var mode
    
    var body: some View {
        VStack(alignment: .leading) {
           HeaderView
           ActionButton
           UserInfo
           TweetFilter
           TweetView
            Spacer()
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}

extension ProfileView {
    var HeaderView: some View {
        ZStack(alignment: .bottomLeading) {
            Color(.systemBlue).ignoresSafeArea()
            VStack {
                Button {
                    mode.wrappedValue.dismiss()
                } label: {
                    Image(systemName:  "arrow.left").resizable().frame(width: 20, height: 16).foregroundColor(.white).offset(x: 16, y:12)
                }
                Circle().frame(width: 72, height: 72).offset(x: 16, y:24)
            }
        }.frame(height: 96)
        
    }
    var ActionButton: some View {
        HStack (spacing: 12) {
            Spacer()
            Image(systemName: "bell.badge").font(.title3).padding(6).overlay(Circle().stroke(Color.gray, lineWidth: 0.75))
            Button {
                
            } label: {
                Text("Edit Profile").font(.subheadline).bold().frame(width: 129, height: 32).overlay(RoundedRectangle(cornerRadius: 20).stroke(Color.gray, lineWidth: 0.75)).foregroundColor(Color.black)
            }
        }.padding(.trailing)
    }
    var UserInfo: some View {
        VStack(alignment: .leading, spacing: 4) {
            HStack {
                Text("Heath Ledger").font(.title2).bold()
                Image(systemName: "checkmark.seal.fill").foregroundColor(Color(.systemBlue))
            }
            Text("@joker").foregroundColor(Color.gray).font(.subheadline)
            Text("Your mom ass").font(.subheadline).padding(.vertical)
            HStack(spacing: 24) {
                HStack {
                    Image(systemName: "mappin.and.ellipse")
                    Text("Gotham, NY")
                }
                HStack {
                    Image(systemName: "link")
                    Text("www.joker.com")
                }
            }.font(.caption).foregroundColor(.gray)
            HStack(spacing: 24) {
                HStack(spacing: 4) {
                    Text("807").bold().font(.subheadline)
                    Text("Following").foregroundColor(.gray).font(.caption)
                }
                HStack {
                    Text("6.9").bold().font(.subheadline)
                    Text("Followers").foregroundColor(.gray).font(.caption)
                }
            }.padding(.vertical)
        }.padding(.horizontal)
    }
    
    var TweetFilter: some View {
        HStack {
            ForEach(TweetFilterViewModel.allCases, id: \.rawValue) {
                option in
                VStack {
                    Text(option.tittle)
                        .font(.subheadline).fontWeight(selectionFilter == option ? .semibold : .regular)
                        .foregroundColor(selectionFilter == option ? .black : .gray)
                    if selectionFilter == option {
                        Capsule().foregroundColor(Color(.systemBlue)).frame( height: 3).matchedGeometryEffect(id: "filter", in: animation)
                        
                    } else {
                        Capsule().foregroundColor(Color(.clear)).frame( height: 3)
                    }
                }.onTapGesture {
                    withAnimation(.easeInOut) {
                        self.selectionFilter = option
                    }
                }
            }
        }.overlay(Divider().offset(x: 0, y: 16))
    }
    var TweetView: some View {
        ScrollView {
            LazyVStack {
                ForEach(0...9, id: \.self) { _ in
                    TweetRowView().padding()
                }
            }
        }
    }
}
