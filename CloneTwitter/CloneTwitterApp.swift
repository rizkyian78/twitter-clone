//
//  CloneTwitterApp.swift
//  CloneTwitter
//
//  Created by Rizky Ian Indiarto on 9/8/22.
//

import SwiftUI

@main
struct CloneTwitterApp: App {
    var body: some Scene {
        WindowGroup {
            MainTabView()
        }
    }
}
